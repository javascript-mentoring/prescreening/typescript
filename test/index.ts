import * as assert from "assert";
import {Converter} from '../src';

describe('Converter', () => {
    it('should return 453.59 for given input {from: "lb", to: "g", value: 1}', () => {
        const result = Converter.convert({from: 'lb', to: 'g', value: 1}, 2);
        assert.strictEqual(result, 453.59);
    });

    it('should return 0.423 for given input {from: "g", to: "oz", value: 12}', () => {
        const result = Converter.convert({from: "g", to: "oz", value: 12}, 3);
        assert.strictEqual(result, 0.423);
    });

    it('should return 36.09 for given input {from: "ft", to: "m", value: 11}', () => {
        const result = Converter.convert({from: "ft", to: "m", value: 11}, 2);
        assert.strictEqual(result, 36.09);
    });

    it('should return 310.928 for given input {from: "F", to: "K", value: 100}', () => {
        const result = Converter.convert({from: "F", to: "K", value: 100}, 3);
        assert.strictEqual(result, 310.928);
    });

    it('should return -272.15 for given input {from: "K", to: "C", value: 1}', () => {
        const result = Converter.convert({from: "K", to: "C", value: 1}, 2);
        assert.strictEqual(result, -272.15);
    });

    it('should convert given input array', () => {
        const input = [
            {from: 'ft', to: 'mi', value: 3000},
            {from: 'K', to: 'F', value: 22},
            {from: 'oz', to: 'lb', value: 143},
            {from: 'mi', to: 'm', value: 12},
        ];
        const expected = [0.57, -420.07, 8.94, 19312.10];
        const result = Converter.convert(input, 2);
        assert.deepStrictEqual(result, expected);
    });
});
