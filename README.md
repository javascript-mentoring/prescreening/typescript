# Typescript PreScreening

## Tasks

### Converter
Your task is to create values converter for temperature, distance and weight. A Converter class should have one method accepting two params:
* `inputObject: {from: string, to: string, value: [number, string]}` as a first param describing value to be converted,
* `numberDecimals: number` as a second param describing how many numbers should be left in the converted value

#### Supported units
|             |   Unit 1 (from/to string value)   |          Unit 2 (from/to string value)        |        Unit 3  (from/to string value)      |
|:-----------:|:----------:|:-----------------------:|:--------------------:|
|    Weight   |  Grams(g)  |  Pound(lb) (453,59 g.)  | Ounce(oz) (28,34 g.) |
| Temperature | Celsius(C) |      Fahrenheit(F)      |       Kelvin(K)      |
|   Distance  |  Meters(m) | Miles(mi) (0.0006214 m) |  Feet(ft) (3.281 m.) |

```js
Converter.convert(
    {
        from: 'lb', to: 'g', value: 1},
        2
    ); // 453.59

Converter.convert(
    [
        {from: 'F', to: 'C', value: 22}, 
        {from: 'C', to: 'F', value: 33}
    ],
    2); // [-5.56, 91.4]
```

## ⚠ Notice
***Main focus of that course is TypeScript so you must declare all types for complex structures, function arguments\return values e.t.c***

***Any types are strictly prohibited!***

Write your code in `src/index.ts.
*All test cases are designed as “error-free”, so don't worry about handling any errors.*

## Prepare and test
1. Install [Node.js](https://nodejs.org/en/download/)   
2. Fork this repository: Typescript
3. Clone your newly created repo: https://gitlab.com/<%your_gitlab_username%>/typescript/  
4. Go to folder typescript  
5. To install all dependencies use [`npm install`](https://docs.npmjs.com/cli/install)  
6. Run `npm test` in the command line  
7. You will see the number of passing and failing tests

## Submit to [AutoCode](https://autocode.lab.epam.com/)
1. Open [AutoCode](https://autocode.lab.epam.com/) and login
2. Subscribe to [JavaScript Mentoring | TypeScript PreScreening](https://autocode.lab.epam.com/student/course/62/group/43)
3. Select your task (JavaScript Mentoring | TypeScript Tasks)
4. Submit your solution to Autocode

### Notes
1. We recommend you to use nodejs of version 12 or lower. If you using are any of the features which are not supported by v12, the score won't be submitted.
2. Each of your test case is limited to 30 sec.
